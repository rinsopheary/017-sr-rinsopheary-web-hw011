import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Card, Form, ListGroup } from "react-bootstrap";
import Calculate from "../images/Caculate.png";
import Result from "./Result";

export default class Calculator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      numbers : []
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    var txt1 = Number(this.t1.value);
    let txt2 = Number(this.t2.value);
    let operator = this.option.value;
    var result;

    switch (operator) {
      case "+":
        result = txt1 + txt2;
        break;
      case "-":
        result = txt1 - txt2;
        break;
      case "x":
        result = txt1 * txt2;
        break;
      case "/":
        result = txt1 / txt2;
        break;
      case "%":
        result = txt1 % txt2;
        break;
      default:
        alert("Please choose operator first!!!");
        break;
    }

    this.setState({
        numbers: this.state.numbers.concat(result),
    });
  }

  render() {

    var item = this.state.numbers.map((ele, index) => (
        <Result output={ele} key={index} />
      ));

    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <div className="container App">
            <div className="row pt-4">
              <div className="col-md-4">
                <Card>
                  <Card.Img variant="top" src={Calculate} />
                  <Card.Body>
                    <Form.Group controlId="formBasicEmail">
                      <Form.Control
                        type="number"
                        placeholder="Enter number"
                        ref={(txt1) => (this.t1 = txt1)}
                        name="t1"
                      />
                    </Form.Group>

                    <Form.Group controlId="formBasicEmail">
                      <Form.Control
                        type="number"
                        placeholder="Enter number"
                        ref={(txt2) => (this.t2 = txt2)}
                        name="t2"
                      />
                    </Form.Group>

                    <Form.Group controlId="">
                      <Form.Control as="select" custom ref={(choose) => (this.option = choose)} name="option">
                        <option value="+">+ &nbsp; : Addition</option>
                        <option value="-">- &nbsp; : Subtraction</option>
                        <option value="x">x &nbsp; : Multiplication</option>
                        <option value="/">/ &nbsp; : Division</option>
                        <option value="%">% : Module</option>
                      </Form.Control>
                    </Form.Group>
                    <Button variant="primary" type="submit">
                      Submit
                    </Button>
                  </Card.Body>
                </Card>
              </div>

              <div className="col-md-4">
              <h1>Result history: </h1> <br/>
              <ListGroup>{item}</ListGroup>
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
