import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { ListGroup } from "react-bootstrap";
import "../index.css";

export default class Result extends Component {
  render() {
    let textAlign = {
      textAlign: "left",
    };
    return (
      <div>
        <ListGroup.Item style={textAlign}>{this.props.output}</ListGroup.Item>
      </div>
    );
  }
}

